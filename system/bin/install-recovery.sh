#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:39617774:0eb75d2c97cd4368a8d18694d8260cc9a893aa60; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25314538:f2d6e38bbc864fca20aaf2eafab7d858b207aa16 EMMC:/dev/block/bootdevice/by-name/recovery 0eb75d2c97cd4368a8d18694d8260cc9a893aa60 39617774 f2d6e38bbc864fca20aaf2eafab7d858b207aa16:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
